# -*- coding: utf-8 -*-
'''
Created on 23/10/2019

@author: Charles
'''

from lamp import Lamp

def run():
    lamp = Lamp(is_turned_on=False)

    while True:
        command = str(input('''
            Que deseas hacer?

            [p]render
            [a]pagar
            [s]alir
        '''))

        if command == 'p':
            lamp.turn_on()
        elif command == 'a':
            lamp.turn_off()
        elif command == 's':
            print('Programa Terminado...')
            break
        else:
            print('Opcion no reconocida...')


if __name__ == '__main__':
    run()